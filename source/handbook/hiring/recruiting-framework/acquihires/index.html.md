---
layout: handbook-page-toc
title: "Recruiting Process - Acquihires"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Recruiting Process Framework - Acquihires
{: #framework-acquihires}

### What is an Acquistion hire?

An Acqusition hire or acquihire is any hire that any hire that is made as a direct results of acquiring a company or acquiring a product. This page outlines the process for all parties privy to the acquition on the recruiting aspects.  

### The Process

1. Create a slack channel with #Acqui-Recruiting-(the name of the acquistion), add all relevant team members to the channel and provide specific details on the confidential nature of the acqusition. 
1. Notify the Recruiting Manager/Lead of the need to process candidates as part of the acquisition at least 2 weeks before the final decision on the candidates needs to be made. 
1. Provide the details of the team they will be joining, the interview process if different from our [Standard Interview process](/handbook/hiring/interviewing/), the hiring manager an director responsible for the process.  
1. Recruiting Manager/Lead to gain a dummy [GHP ID](/handbook/finance/financial-planning-and-analysis/#headcount-and-the-recruiting-single-source-of-truth)
1. Recruiting Manager/Lead will open a Requistion in Greenhouse that will house all the acquisition candidates. 
1. Recruiting Manager will assign the Requsition to a Recruiter who will manage the Interview process. 
1. When a acquisition candidate has completed the process, the recruiter responsible will update the Slack Channel with the results. 
1. Once complete we will follow the standard process of background checks, offer approvals and offer process as outlined here [Hiring](/handbook/hiring/)
