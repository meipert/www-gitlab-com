---
layout: handbook-page-toc
<<<<<<< HEAD
title: "Verify (CI) / Stage Adoption Playbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [Customer Success homepage](/handbook/customer-success/) for additional Customer Success handbook content.

---

The following steps are the steps for the Verify/CI adoption play held and activated within Gainsight for TAMs:

## Discovery

[The questions found here](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/#discovery-questions) are recommended questions for discovering customer needs.
=======
title: "Continuous Integration (CI) / Verify Workshop"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [Customer Success homepage](/handbook/customer-success/) for additional Customer Success handbook content.

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


View the [Customer Success homepage](/handbook/customer-success/) for additional Customer Success handbook content.

The following items are for the CI/Verify workshop that the TAM team will deliver to the customer in conjunction with the SA and sometimes PS as half-day, free workshop to enable the customer to see first-hand how easy CI is with Gitab:

---
>>>>>>> master

## CI/Verify Workshop

### Workshop Deliverables

<<<<<<< HEAD
### Conversion
=======
**Project Conversion**
>>>>>>> master

Create a project plan for converting a project. Rough outline:

1. Fork a project to GitLab.com and make it private.
2. Add the customer and members of this team to the project.
3. Convert the project to GitLab CI.Create a working .gitlab-ci.yml file.

<<<<<<< HEAD
### Enablement

Resources:
=======
**Resources:**
>>>>>>> master

[GitLab CI Enablement Deck - Best Practices in Migrating from Jenkins](https://docs.google.com/presentation/d/1eR_874yUHu5Yz8jC-7Gwtiz9j8N4APlgz7NT1_UR0mE/edit#slide=id.g849e6d84e3_0_636) (GitLab internal WIP)

[Adopting GitLab CI at scale doc](https://docs.google.com/document/d/19oKupXi_nnFwD0VOilMhTH2nzUvrBN3P9hI-R5c6P8w/edit#heading=h.b61novry8f4t) (GitLab internal WIP)

Key Topics:

1. Work with the customer’s users to enable them on GitLab CI/CD.
2. How to write a .gitlab-ci.yml file.
3. How to use the GitLab (Shared) Runners.
4. How to create templates for standardization & quick scaling
<<<<<<< HEAD

## Adoption

- Product documentation (Content owner: Product and Engineering Teams)
- Training assets
- Paid services
- Adoption Map

| Feature / Use Case | F/C  | Basic | S/P  | G/U  |
| ------------------ | ---- | ----- | ---- | ---- |
|                    |      |       |      |      |
|                    |      |       |      |      |
|                    |      |       |      |      |

The table includes free/community and paid tiers associated with GitLab's self-managed and cloud offering.

- F/C = Free / Core
- Basic = Bronze/Starter
- S/P = Silver / Premium
- G/U = Gold / Ultimate

## Reporting and Metrics

- Link to telemetry metrics

## Learning Resources

- [CI Customer Use Case](/handbook/marketing/product-marketing/usecase-gtm/ci/#continuous-integration)
- [GitLab Journey and CI Use Case](https://www.youtube.com/watch?v=JoeaTYIH5lI&feature=youtu.be)
- [CS Skills Exchnage: CI Deep Dive](https://www.youtube.com/watch?v=ZVUbmVac-m8&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=3&t=0s)
- [CS Skills Exchange: Runners Overview](https://www.youtube.com/watch?v=JFMXe1nMopo&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=11&t=0s)
- [GitLab Runners Deep Dive](https://www.youtube.com/watch?v=GPSkNang9AU&list=PL05JrBw4t0KorkxIFgZGnzzxjZRCGROt_&index=19&t=0s)
- [Technically Competing Against Microsoft Azure DevOps](https://drive.google.com/open?id=18jwSeeUylGXv8LoEedCMRfBZt9t7QLOYKCHJp-SvdqA)
- [Competing Against Jenkins](https://drive.google.com/open?id=1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg)
- [DevOps Lifecycle: CI and CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)
- Coming Soon: CI Learning Path
=======
>>>>>>> master
